//
//  AVWeatherRequestBase.m
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import "AVWeatherRequestBase.h"
#import "AVNetworkRequestsManager.h"

@interface AVWeatherRequestBase ()

@property (nonatomic, assign) NSUInteger cityID;
@property (nonatomic, strong) NSDictionary *options;

@end

@implementation AVWeatherRequestBase

@synthesize delegate;
@synthesize requestsManager;

- (instancetype)initWithOptions:(NSDictionary *)anOptions requestsManager:
			(AVNetworkRequestsManager *)aManager
{
	self = [super init];
	if (nil != self)
	{
		self.options = anOptions;
		self.cityID = [anOptions[AVCityIDOptionsKey] integerValue];
		self.requestsManager = aManager;
	}
	
	return self;
}

- (void)start
{
	NSString *theURLPath = [NSString stringWithFormat:[self endPointFormatedString], self.cityID,
				kAVAPIToken];
	NSURL *theURL = [NSURL URLWithString:theURLPath];
	NSURLSessionConfiguration *theSessionConfiguration = [NSURLSessionConfiguration
				defaultSessionConfiguration];
	NSURLSession *theSession = [NSURLSession sessionWithConfiguration:theSessionConfiguration];
	NSURLRequest *theRequest = [NSURLRequest requestWithURL:theURL];
   NSURLSessionDataTask *theDataTask = [theSession dataTaskWithRequest:theRequest completionHandler:
	^(NSData *aData, NSURLResponse *aResponse, NSError *anError)
	{
		id theResult = nil;
		if (nil == anError)
		{
			if (aData.length > 0)
			{
				theResult = [self foundationObjectForData:aData];
			}
		}
		else
		{
			// if no Internet-related error - try after timeout
			if ([NSURLSession isBadConnectionError:anError])
			{
				[self.requestsManager repeatAfterDelayProcess:self];
			}
		}
		dispatch_async(dispatch_get_main_queue(),
		^{
			[self.requestsManager didFinishRequest:self];
			[self.delegate request:self didFinishWithResult:theResult options:self.options
						error:anError];
		});
	}];
 
    [theDataTask resume];
}

- (AVRequestType)type
{
	return AVUndefinedRequestType;
}

- (id)foundationObjectForData:(NSData *)aData
{
	// Should be implemented by a subclass
	[self doesNotRecognizeSelector:_cmd];
	return nil;
}

- (NSString *)endPointFormatedString
{
	// Should be implemented by a subclass
	[self doesNotRecognizeSelector:_cmd];
	return nil;
}

- (BOOL)isEqualToRequest:(id<AVWeatherRequest>)aRequest;
{
	return (([self type] == [aRequest type]) && (self.cityID ==
				[(AVWeatherRequestBase *)aRequest cityID]));
}

@end
