//
//  AVNetworkRequestsManager.h
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AVNetworkRequestsManager;
@protocol AVRequestDelegate;

typedef NS_ENUM(NSUInteger, AVRequestType)
{
	AVUndefinedRequestType = 0,
	AVCurrentWeatherRequestType,
	kAVWeatherForecastRequestType,
	kAVWeatherIconRequestType
};

extern NSString *const AVCityIDOptionsKey;
extern NSString *const kAVWeatherIconNameOptionsKey;

extern NSString *const kAVAPIToken;

@protocol AVWeatherRequest <NSObject>

@property (nonatomic, weak) id<AVRequestDelegate> delegate;
@property (nonatomic, weak) AVNetworkRequestsManager *requestsManager;

- (instancetype)initWithOptions:(NSDictionary *)anOptions requestsManager:
			(AVNetworkRequestsManager *)aManager;
- (void)start;
- (AVRequestType)type;
- (NSString *)endPointFormatedString;
- (id)foundationObjectForData:(NSData *)aData;
- (BOOL)isEqualToRequest:(id<AVWeatherRequest>)aRequest;

@end

@protocol AVRequestDelegate

- (void)request:(id<AVWeatherRequest>)aRequest didFinishWithResult:(id)aResult
			options:(NSDictionary *)anOptions error:(NSError *)anError;

@end

@interface NSURLSession (ErrorTypeCategory)

+ (BOOL)isBadConnectionError:(NSError *)anError;

@end

@interface AVNetworkRequestsManager : NSObject

+ (instancetype)sharedManager;

- (id<AVWeatherRequest>)weatherRequestWithOptions:(NSDictionary *)anOptions type:
			(AVRequestType)aType;
- (void)scheduleRequest:(id<AVWeatherRequest>)aRequest delegate:(id<AVRequestDelegate>)aDelegate;
- (void)didFinishRequest:(id<AVWeatherRequest>)aRequest;
- (void)repeatAfterDelayProcess:(id<AVWeatherRequest>)aRequest;

@end
