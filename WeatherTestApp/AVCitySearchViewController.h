//
//  AVCitySearchViewController.h
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AVAsynchronousModel.h"

@class AVCitySearchViewController;
@class AVCity;

@protocol AVCitySearchDelegate <NSObject>

- (void)citySearchController:(AVCitySearchViewController *)aController didSelectCity:
			(AVCity *)aCity;

@end

@interface AVCitySearchViewController : UIViewController <UISearchBarDelegate,
			UISearchControllerDelegate, UISearchResultsUpdating, UITableViewDataSource,
			UITableViewDelegate, AVUpdatingControllerProtocol>

@property (weak, nonatomic) id<AVCitySearchDelegate> delegate;

@end
