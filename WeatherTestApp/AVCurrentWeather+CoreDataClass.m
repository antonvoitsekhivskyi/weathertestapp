//
//  AVCurrentWeather+CoreDataClass.m
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import "AVCurrentWeather+CoreDataClass.h"
#import "AVCity+CoreDataProperties.h"

NSString *const kAVCurrentWeatherEntityName = @"AVCurrentWeather";

@implementation AVCurrentWeather

@end
