//
//  AVWeatherForecastViewController.m
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import "AVWeatherForecastViewController.h"
#import "AVWeatherForecast+CoreDataProperties.h"
#import "AVCurrentWeather+CoreDataProperties.h"
#import "AVCity+CoreDataProperties.h"
#import "AVWeatherForecastTableViewCell.h"
#import "AVWeatherForecastModel.h"

@interface AVWeatherForecastViewController ()

@property (nonatomic, strong) AVWeatherForecastModel *model;
@property (nonatomic, assign) BOOL hasNotifiedAboutError;

@end

@implementation AVWeatherForecastViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	self.model = [[AVWeatherForecastModel alloc] initWithCity:self.city controller:self];
	self.tableView.rowHeight = UITableViewAutomaticDimension;
	self.tableView.estimatedRowHeight = 220;
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
}

- (void)updateWeather
{
	[self.tableView reloadData];
}

#pragma mark - Managing the detail item

- (void)setCity:(AVCity *)aNewCity
{
	if (_city != aNewCity)
	{
		self.hasNotifiedAboutError = NO;
		_city = aNewCity;
		self.model.city = aNewCity;
		[self.tableView reloadData];
	}
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)aSection
{
	// Current day + 3 days forecast
	return self.model.city == nil ? 0 : 4;
}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)
			anIndexPath
{
	AVWeatherForecastTableViewCell *theResult = nil;
	if (0 == anIndexPath.row)
	{
		AVCurrentWeather *theCurrentForecast = [self.model currentWeatherForecast];
		theResult = [aTableView dequeueReusableCellWithIdentifier:@"AVCurrentWeatherCell"];
		
		if (nil == theCurrentForecast)
		{
			[theResult beginIndeterminateProgress];
		}
		else
		{
			theResult.cityLabel.text = self.city.name;
			theResult.temperatureLabel.text = [NSString stringWithFormat:@"%d°С",
						(int)roundf(theCurrentForecast.temperature)];
			theResult.cloudsDescription.text = theCurrentForecast.cloud;
			theResult.preasureLabel.text = [NSString stringWithFormat:@"%d hPa",
						(int)roundf(theCurrentForecast.pressure)];
			theResult.windLabel.text = [NSString stringWithFormat:@"%d %@",
						(int)roundf(theCurrentForecast.windSpeed), @"m/s"];
			theResult.humidityLabel.text = [NSString stringWithFormat:@"%d%@",
						theCurrentForecast.humidity, @"%"];
			theResult.cloudsImageView.image = [self.model imageForIconName:theCurrentForecast.iconName];
			[theResult stopIndeterminateProgress];
		}
	}
	else
	{
		AVWeatherForecast *theForecast = [self.model weatherForecastForDay:anIndexPath.row];
		theResult = [aTableView dequeueReusableCellWithIdentifier:@"AVForecastWeatherCell"];

		if (nil == theForecast)
		{
			[theResult beginIndeterminateProgress];
		}
		else
		{
			theResult.temperatureLabel.text = [NSString stringWithFormat:@"%d°С - %d°С",
						(int)roundf(theForecast.minTemperature),
						(int)roundf(theForecast.maxTemperature)];
			NSDateFormatter *theFormatter = [NSDateFormatter new];
			[theFormatter setDateFormat:@"dd/MM/yyyy"];
			theResult.dateLabel.text = [theFormatter stringFromDate:theForecast.date];
			theResult.cloudsImageView.image = [self.model imageForIconName:theForecast.iconName];
			[theResult stopIndeterminateProgress];
		}
	}
	return theResult;
}

- (void)showErrorAlert
{
// Notify if there was no previous alert for current city.
	if (!self.hasNotifiedAboutError)
	{
		self.hasNotifiedAboutError = YES;
		UIAlertController *theAlertController = [UIAlertController alertControllerWithTitle:
					@"Connection Problem" message:@"Check Your Internet Connection" preferredStyle:
					UIAlertControllerStyleAlert];
		UIAlertAction *theOKAction = [UIAlertAction actionWithTitle:@"OK"
					style:UIAlertActionStyleDefault handler:NULL];
		[theAlertController addAction:theOKAction];
		[self presentViewController:theAlertController animated:YES completion:NULL];
	}
}

@end
