//
//  AVCitiesListViewController.m
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import "AVCitiesListViewController.h"
#import "AVWeatherForecastViewController.h"
#import "AVCitySearchViewController.h"
#import "AVCitiesListModel.h"
#import "AVCity+CoreDataProperties.h"

static NSString *const AVShowCitySearchSegueIdentifier = @"AVShowCitySearchSegue";
static NSString *const AVShowWeatherForecastSegueIdentifier = @"AVShowWeatherForecastSegue";

@interface AVCitiesListViewController ()

@property (nonatomic, strong) AVCitiesListModel *model;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation AVCitiesListViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
}

- (AVCitiesListModel *)model
{
	if (nil == _model)
	{
		_model = [[AVCitiesListModel alloc] initWithController:self];
		[_model filterCitiesUsingString:self.searchDisplayController.searchBar.text];
	}
	return _model;
}

- (void)viewWillAppear:(BOOL)anAnimated
{
	[super viewWillAppear:anAnimated];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
}

#pragma mark - AVUpdatingControllerProtocol implementation

- (void)updateContents
{
	if ([self.searchDisplayController isActive])
	{
		[self.searchDisplayController.searchResultsTableView reloadData];
	}
	else
	{
		[UIView animateWithDuration:0.3 animations:
		^{
			self.tableView.alpha = (0 == self.model.cities.count) ? 0.0 : 1.0;
		 }];
		[self.tableView reloadData];
	}
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)aSegue sender:(id)aSender
{
	if ([[aSegue identifier] isEqualToString:AVShowWeatherForecastSegueIdentifier])
	{
		NSIndexPath *anIndexPath = nil;
		anIndexPath = (self.searchDisplayController.isActive) ?
					[self.searchDisplayController.searchResultsTableView indexPathForSelectedRow] :
					[self.tableView indexPathForSelectedRow];
		AVCity *theSelectedCity = self.model.cities[anIndexPath.row];
		AVWeatherForecastViewController *aController = (AVWeatherForecastViewController *)[[aSegue
					destinationViewController] topViewController];
		aController.city = theSelectedCity;
		aController.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
		aController.navigationItem.leftItemsSupplementBackButton = YES;
	}
	else if ([aSegue.identifier isEqualToString:AVShowCitySearchSegueIdentifier])
	{
		[(AVCitySearchViewController *)aSegue.destinationViewController setDelegate:self];
	}
}

#pragma mark - AVCitySearchDelegate protocol implemetnation

- (void)citySearchController:(AVCitySearchViewController *)aController didSelectCity:(AVCity *)aCity
{
	[self.model addCity:aCity];
//	[self.model filterCitiesUsingString:self.model.currentFilter];
}

#pragma mark - Table View

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)aSection
{
	return self.model.cities.count;
}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:
			(NSIndexPath *)anIndexPath
{
	static NSString *theCellIdentifier = @"AVCityListCell";
	UITableViewCell *theCell = [aTableView dequeueReusableCellWithIdentifier:
				theCellIdentifier];
	if (nil == theCell)
	{
		theCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
					reuseIdentifier:theCellIdentifier];
	}
	theCell.textLabel.text = self.model.cities[anIndexPath.row].name;
	theCell.detailTextLabel.text = self.model.cities[anIndexPath.row].country;
	
	return theCell;
}

- (BOOL)tableView:(UITableView *)aTableView canEditRowAtIndexPath:(NSIndexPath *)anIndexPath
{
	return YES;
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)anIndexPath
{
	[self performSegueWithIdentifier:AVShowWeatherForecastSegueIdentifier sender:[aTableView cellForRowAtIndexPath:anIndexPath]];
}

- (void)tableView:(UITableView *)aTableView commitEditingStyle:(UITableViewCellEditingStyle)
			anEditingStyle forRowAtIndexPath:(NSIndexPath *)anIndexPath
{
	if (anEditingStyle == UITableViewCellEditingStyleDelete)
	{
		[self.model removeCity:self.model.cities[anIndexPath.row]];
//		[self.model filterCitiesUsingString:self.searchDisplayController.searchBar.text];
	}
}

#pragma mark - Seach Display Controller

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller
			shouldReloadTableForSearchString:(nullable NSString *)searchString
{
	[self.model filterCitiesUsingString:searchString];
	return YES;
}

- (void)searchDisplayController:(UISearchDisplayController *)controller
			willHideSearchResultsTableView:(nonnull UITableView *)aTableView
{
	[self.model filterCitiesUsingString:@""];
}

@end
