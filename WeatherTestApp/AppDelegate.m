//
//  AppDelegate.m
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import "AppDelegate.h"
#import "AVWeatherForecastViewController.h"
#import "AVCitiesListViewController.h"

@interface AppDelegate () <UISplitViewControllerDelegate>

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)anApplication didFinishLaunchingWithOptions:
			(NSDictionary *)aLaunchOptions
{
	UISplitViewController *theSplitViewController =
				(UISplitViewController *)self.window.rootViewController;
	UINavigationController *theNavigationController = [theSplitViewController.viewControllers
				lastObject];
	theNavigationController.topViewController.navigationItem.leftBarButtonItem =
				theSplitViewController.displayModeButtonItem;
	theSplitViewController.delegate = self;

	return YES;
}

- (void)applicationWillResignActive:(UIApplication *)anApplication
{

}

- (void)applicationDidEnterBackground:(UIApplication *)anApplication
{

}

- (void)applicationWillEnterForeground:(UIApplication *)anApplication
{

}

- (void)applicationDidBecomeActive:(UIApplication *)anApplication
{

}

- (void)applicationWillTerminate:(UIApplication *)anApplication
{
	[[self managedObjectContext] save:nil];
}

#pragma mark - Split view

- (BOOL)splitViewController:(UISplitViewController *)aSplitViewController
			collapseSecondaryViewController:(UIViewController *)aSecondaryViewController
			ontoPrimaryViewController:(UIViewController *)aPrimaryViewController
{
   if ([aSecondaryViewController isKindOfClass:[UINavigationController class]] &&
				[[(UINavigationController *)aSecondaryViewController topViewController] isKindOfClass:
				[AVWeatherForecastViewController class]] && ([(AVWeatherForecastViewController *)
				[(UINavigationController *)aSecondaryViewController topViewController] city]
				== nil))
	{
		// Return YES to indicate that we have handled the collapse by doing nothing; the secondary
		  // controller will be discarded.
		return YES;
   }
	else
	{
		return NO;
   }
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory
{
	return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
				inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel
{
	@synchronized(self)
	{
		if (_managedObjectModel != nil)
		{
			return _managedObjectModel;
		}
		NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"WeatherTestApp" withExtension:@"momd"];
		_managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
		return _managedObjectModel;
	}
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
	@synchronized(self)
	{
		if (_persistentStoreCoordinator != nil)
		{
			return _persistentStoreCoordinator;
		}
		
		// Create the coordinator and store
		
		_persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]
					initWithManagedObjectModel:[self managedObjectModel]];
		NSURL *theStoreURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:
					@"WeatherTestApp.sqlite"];

		if (![[NSFileManager defaultManager] fileExistsAtPath:[theStoreURL path]])
		{
			 NSURL *theBundleDataBaseURL = [NSURL fileURLWithPath:[[NSBundle mainBundle]
						 pathForResource: @"WeatherTestApp" ofType:@"sqlite"]];
			[[NSFileManager defaultManager] copyItemAtURL:theBundleDataBaseURL toURL:theStoreURL
						error:nil];
		}
		
		NSError *error = nil;
		NSString *failureReason = @"There was an error creating or loading the application's saved data.";
		if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:theStoreURL options:nil error:&error])
		{
			// Report any error we got.
			NSMutableDictionary *dict = [NSMutableDictionary dictionary];
			dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
			dict[NSLocalizedFailureReasonErrorKey] = failureReason;
			dict[NSUnderlyingErrorKey] = error;
			error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
			// Replace this with code to handle the error appropriately.
			// abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
			NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
			abort();
		}
		
		return _persistentStoreCoordinator;
	}
}

- (NSManagedObjectContext *)managedObjectContext
{
	@synchronized(self)
	{
		// Returns the managed object context for the application (which is already bound to the
		// persistent store coordinator for the application.)
		if (_managedObjectContext != nil)
		{
			return _managedObjectContext;
		}
		
		NSPersistentStoreCoordinator *theCoordinator = [self persistentStoreCoordinator];
		if (!theCoordinator)
		{
			return nil;
		}
		_managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:
					NSMainQueueConcurrencyType];
		[_managedObjectContext setPersistentStoreCoordinator:theCoordinator];
		return _managedObjectContext;
	}
}

@end
