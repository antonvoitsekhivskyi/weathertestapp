//
//  AVWeather+CoreDataClass.h
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface AVWeather : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "AVWeather+CoreDataProperties.h"
