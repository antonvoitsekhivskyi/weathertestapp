//
//  AVCity+CoreDataClass.m
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import "AVCity+CoreDataClass.h"
#import "AVWeatherForecast+CoreDataProperties.h"

NSString *const kAVCityEntityName = @"AVCity";

@implementation AVCity

@end
