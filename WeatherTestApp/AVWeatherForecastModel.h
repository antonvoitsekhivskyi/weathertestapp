//
//  AVWeatherForecastModel.h
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AVNetworkRequestsManager.h"
#import <UIKit/UIKit.h>

@class AVCity;
@class AVCurrentWeather;
@class AVWeatherForecast;
@class AVWeatherForecastViewController;

@interface AVWeatherForecastModel : NSObject <AVRequestDelegate>

@property (nonatomic, strong) AVCity *city;

- (instancetype)initWithCity:(AVCity *)aCity controller:
			(AVWeatherForecastViewController *)aController;

- (AVCurrentWeather *)currentWeatherForecast;
- (AVWeatherForecast *)weatherForecastForDay:(NSUInteger)aDay;

- (UIImage *)imageForIconName:(NSString *)anIconName;

@end
