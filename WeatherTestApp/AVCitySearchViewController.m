//
//  AVCitySearchViewController.m
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import "AVCitySearchViewController.h"
#import "AVCitySearchModel.h"
#import "AVCity+CoreDataProperties.h"

@interface AVCitySearchViewController ()

@property (nonatomic, strong) AVCitySearchModel *model;
@property (nonatomic, strong) UISearchController *searchController;
@property (strong, nonatomic) UITableViewController *resultsTableController;

@end

@implementation AVCitySearchViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	self.model = [[AVCitySearchModel alloc] initWithController:self];
	
	self.resultsTableController = [UITableViewController new];
	self.resultsTableController.tableView.dataSource = self;
	self.resultsTableController.tableView.delegate = self;
	_searchController = [[UISearchController alloc] initWithSearchResultsController:
				self.resultsTableController];
	self.searchController.searchResultsUpdater = self;
	[self.searchController.searchBar sizeToFit];

	self.searchController.hidesNavigationBarDuringPresentation = NO;
	self.searchController.dimsBackgroundDuringPresentation = YES;

	self.navigationItem.titleView = self.searchController.searchBar;

	self.definesPresentationContext = YES;
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:
			(NSIndexPath *)anIndexPath
{
	static NSString *theCellIdentifier = @"AVCitySearchCell";
	UITableViewCell *theCell = [aTableView dequeueReusableCellWithIdentifier:
				theCellIdentifier];
	if (nil == theCell)
	{
		theCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
					reuseIdentifier:theCellIdentifier];
	}
	theCell.textLabel.text = self.model.cities[anIndexPath.row].name;
	theCell.detailTextLabel.text = self.model.cities[anIndexPath.row].country;
	
	return theCell;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)aSection
{
	return self.model.cities.count;
}

- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)anIndexPath
{
	[self.delegate citySearchController:self didSelectCity:
				self.model.cities[anIndexPath.row]];
	[self.navigationController popToViewController:(UIViewController *)self.delegate animated:YES];
}

- (void)updateContents
{
	[self.resultsTableController.tableView reloadData];	
}

#pragma mark - UISearchResultsUpdating protocol implementation

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
	if (self.searchController.searchBar.text.length > 0)
	{
		[self.model filterCitiesUsingString:self.searchController.searchBar.text];
	}
}

@end
