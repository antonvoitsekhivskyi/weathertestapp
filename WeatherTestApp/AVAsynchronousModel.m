//
//  AVAsynchronousModel.m
//  WeatherTestApp
//
//  Created by Family Mac on 24/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import "AVAsynchronousModel.h"
#import "AVCity+CoreDataProperties.h"
#import "AppDelegate.h"

@interface AVAsynchronousModel ()

@property (nonatomic, strong) NSArray <AVCity *> *cities;
@property (nonatomic, strong) NSString *currentFilter;
@property (nonatomic, weak) id<AVUpdatingControllerProtocol> controller;

@end

@implementation AVAsynchronousModel

- (instancetype)initWithController:(id<AVUpdatingControllerProtocol>)aController
{
	self = [super init];
	
	if (nil != self)
	{
		self.controller = aController;
	}
	return self;
}

- (void)filterCitiesUsingString:(NSString *)aFilterString
{
	self.currentFilter = aFilterString;
	NSPredicate *thePredicate = [self filteringPredicate];
	
	NSFetchRequest *theCitiesFetchRequest = [AVCity fetchRequest];
	theCitiesFetchRequest.predicate = thePredicate;
    __weak AVAsynchronousModel *weakSelf = self;	
   NSAsynchronousFetchRequest *theAsynchronousFetchRequest = [[NSAsynchronousFetchRequest alloc]
				initWithFetchRequest:theCitiesFetchRequest completionBlock:
	^(NSAsynchronousFetchResult *aResult)
	{
		dispatch_async(dispatch_get_main_queue(),
		^{
			weakSelf.cities = aResult.finalResult;
			[weakSelf.controller updateContents];
      });
    }];

	[[self managedObjectContext] performBlock:
	^{
		[[weakSelf managedObjectContext] executeRequest:theAsynchronousFetchRequest error:nil];
	}];
}

- (NSManagedObjectContext *)managedObjectContext
{
	return [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
}

-(NSPredicate *)filteringPredicate
{
	// Should be implemented by a subclass
	[self doesNotRecognizeSelector:_cmd];
	return nil;
}

@end
