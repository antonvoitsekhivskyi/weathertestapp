//
//  AVWeatherForecast+CoreDataProperties.m
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import "AVWeatherForecast+CoreDataProperties.h"

@implementation AVWeatherForecast (CoreDataProperties)

+ (NSFetchRequest<AVWeatherForecast *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"AVWeatherForecast"];
}

@dynamic date;
@dynamic city;

@end
