//
//  AVCurrentWeather+CoreDataProperties.h
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import "AVCurrentWeather+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface AVCurrentWeather (CoreDataProperties)

+ (NSFetchRequest<AVCurrentWeather *> *)fetchRequest;

@property (nonatomic) float temperature;
@property (nullable, nonatomic, retain) AVCity *city;

@end

NS_ASSUME_NONNULL_END
