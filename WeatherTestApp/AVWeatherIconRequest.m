//
//  AVWeatherIconRequest.m
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import "AVWeatherIconRequest.h"
#import <UIKit/UIKit.h>

static NSString *const kAVWeaterImageRequestEndpoint = @"http://openweathermap.org/img/w/%@";

@interface AVWeatherIconRequest ()

@property (nonatomic, strong) NSString *iconName;
@property (nonatomic, strong) NSDictionary *options;

@end

@implementation AVWeatherIconRequest

@synthesize delegate;
@synthesize requestsManager;

- (instancetype)initWithOptions:(NSDictionary *)anOptions requestsManager:
			(AVNetworkRequestsManager *)aManager
{
	self = [super init];
	if (nil != self)
	{
		self.iconName = anOptions[kAVWeatherIconNameOptionsKey];
		self.options = anOptions;
		self.requestsManager = aManager;
	}
	
	return self;
}

- (void)start
{
	NSString *theURLPath = [NSString stringWithFormat:[self endPointFormatedString], self.iconName];
	NSURL *theURL = [NSURL URLWithString:theURLPath];
	NSURLSessionConfiguration *theSessionConfiguration = [NSURLSessionConfiguration
				defaultSessionConfiguration];
	NSURLSession *theSession = [NSURLSession sessionWithConfiguration:theSessionConfiguration];
	NSURLRequest *theRequest = [NSURLRequest requestWithURL:theURL];
   NSURLSessionDownloadTask *theDataTask = [theSession downloadTaskWithRequest:theRequest
				completionHandler:
	^(NSURL * _Nullable aLocation, NSURLResponse * _Nullable aResponse, NSError * _Nullable anError)
	{
		UIImage *theResult = nil;
		if (nil != aLocation)
		{
			NSString *theDocumentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
						NSUserDomainMask, YES) firstObject];
			NSURL *theDocumentsDirectoryURL = [NSURL fileURLWithPath:theDocumentsPath];
			NSURL *thePictureURL = [theDocumentsDirectoryURL URLByAppendingPathComponent:
						self.iconName];
			NSError *theError = nil;
			[[NSFileManager defaultManager] moveItemAtURL:aLocation toURL:thePictureURL error:
						&theError];
			theResult = [UIImage imageWithContentsOfFile:thePictureURL.path];
			dispatch_async(dispatch_get_main_queue(),
			^{

			});
		}
		else
		{
			// if not an Internet-related error - try after timeout
			if ([NSURLSession isBadConnectionError:anError])
			{
				[self.requestsManager repeatAfterDelayProcess:self];
			}
		}
		dispatch_async(dispatch_get_main_queue(),
		^{
			[self.requestsManager didFinishRequest:self];
			[self.delegate request:self didFinishWithResult:theResult options:self.options
						error:anError];
		});
	}];
 
	[theDataTask resume];
}

- (AVRequestType)type
{
	return kAVWeatherIconRequestType;
}

- (NSString *)endPointFormatedString
{
	return kAVWeaterImageRequestEndpoint;
}

- (BOOL)isEqualToRequest:(id<AVWeatherRequest>)aRequest;
{
	return (([self type] == [aRequest type]) && ([self.iconName isEqualToString:
				[(AVWeatherIconRequest *)aRequest iconName]]));
}

- (id)foundationObjectForData:(NSData *)aData
{
	return nil;
}

@end
