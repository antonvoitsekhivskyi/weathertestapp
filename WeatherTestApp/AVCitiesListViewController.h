//
//  AVCitiesListViewController.h
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AVCitySearchViewController.h"
#import "AVAsynchronousModel.h"

@class AVWeatherForecastViewController;

@interface AVCitiesListViewController : UIViewController<UITableViewDataSource,
			UISearchDisplayDelegate, AVCitySearchDelegate, AVUpdatingControllerProtocol>

@property (strong, nonatomic) AVWeatherForecastViewController *AVWeatherForecastViewController;
@property (strong, nonatomic) IBOutlet UISearchDisplayController *searchDisplayController;

@end

