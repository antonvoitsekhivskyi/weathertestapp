//
//  AVWeatherForecast+CoreDataClass.h
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "AVWeather+CoreDataClass.h"

extern NSString *const kAVWeatherForecastEntityName;

@class AVCity;

NS_ASSUME_NONNULL_BEGIN

@interface AVWeatherForecast : AVWeather

@end

NS_ASSUME_NONNULL_END

#import "AVWeatherForecast+CoreDataProperties.h"
