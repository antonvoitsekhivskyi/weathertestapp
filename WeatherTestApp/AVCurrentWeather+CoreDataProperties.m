//
//  AVCurrentWeather+CoreDataProperties.m
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import "AVCurrentWeather+CoreDataProperties.h"

@implementation AVCurrentWeather (CoreDataProperties)

+ (NSFetchRequest<AVCurrentWeather *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"AVCurrentWeather"];
}

@dynamic temperature;
@dynamic city;

@end
