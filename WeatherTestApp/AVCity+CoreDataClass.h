//
//  AVCity+CoreDataClass.h
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

extern NSString *const kAVCityEntityName;


NS_ASSUME_NONNULL_BEGIN

@interface AVCity : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "AVCity+CoreDataProperties.h"
