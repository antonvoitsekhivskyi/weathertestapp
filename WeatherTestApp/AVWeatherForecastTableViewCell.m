//
//  AVWeatherForecastTableViewCell.m
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import "AVWeatherForecastTableViewCell.h"

@interface AVWeatherForecastTableViewCell ()

@property (weak, nonatomic) IBOutlet UIView *indeterminateView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indeterminateProgress;

@end

@implementation AVWeatherForecastTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)beginIndeterminateProgress
{
	[self.indeterminateProgress startAnimating];
	[UIView animateWithDuration:0.3 animations:
	^{
		self.indeterminateView.alpha = 1.0;
	}];
}

- (void)stopIndeterminateProgress
{
	[self.indeterminateProgress stopAnimating];
	[UIView animateWithDuration:0.3 animations:
	^{
		self.indeterminateView.alpha = 0.0;
	}];
}

@end
