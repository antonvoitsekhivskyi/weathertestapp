//
//  AVWeather+CoreDataProperties.m
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import "AVWeather+CoreDataProperties.h"

@implementation AVWeather (CoreDataProperties)

+ (NSFetchRequest<AVWeather *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"AVWeather"];
}

@dynamic cloud;
@dynamic humidity;
@dynamic maxTemperature;
@dynamic minTemperature;
@dynamic pressure;
@dynamic updateDate;
@dynamic windSpeed;
@dynamic iconName;

@end
