//
//  AVWeatherForecastRequest.m
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import "AVWeatherForecastRequest.h"
#import "AVNetworkRequestsManager.h"

static NSString *const kAVWeaterForecastRequestEndpoint =
			@"http://api.openweathermap.org/data/2.5/forecast/daily?id=%lu&units=metric&cnt=4&APPID=%@";

@implementation AVWeatherForecastRequest 

- (NSString *)endPointFormatedString
{
	return kAVWeaterForecastRequestEndpoint;
}

- (id)foundationObjectForData:(NSData *)aData
{
	NSDictionary *theResult = [NSJSONSerialization JSONObjectWithData:aData options:0 error:nil];
	
	return theResult;
}

- (AVRequestType)type
{
	return kAVWeatherForecastRequestType;
}


@end
