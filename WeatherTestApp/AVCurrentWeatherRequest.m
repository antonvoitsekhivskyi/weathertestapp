//
//  AVCurrentWeatherRequest.m
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import "AVCurrentWeatherRequest.h"
#import "AVNetworkRequestsManager.h"

static NSString *const kAVCurrentWeaterRequestEndpoint = @"http://api.openweathermap.org/data/2.5/weather?id=%lu&units=metric&APPID=%@";

@implementation AVCurrentWeatherRequest

- (NSString *)endPointFormatedString
{
	return kAVCurrentWeaterRequestEndpoint;
}

- (id)foundationObjectForData:(NSData *)aData
{
	NSDictionary *theResult = [NSJSONSerialization JSONObjectWithData:aData options:0 error:nil];
	
	return theResult;
}

- (AVRequestType)type
{
	return AVCurrentWeatherRequestType;
}

@end
