//
//  AVCitiesListModel.h
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AVAsynchronousModel.h"

@class AVCity;
@class AVCitiesListViewController;

@interface AVCitiesListModel : AVAsynchronousModel

- (void)addCity:(AVCity *)aCity;
- (void)removeCity:(AVCity *)aCity;

@end
