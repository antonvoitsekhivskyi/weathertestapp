//
//  AVWeatherForecastViewController.h
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AVCity;

@interface AVWeatherForecastViewController : UITableViewController

@property (strong, nonatomic) AVCity *city;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

- (void)updateWeather;
- (void)showErrorAlert;

@end

