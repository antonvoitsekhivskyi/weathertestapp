//
//  AVCitySearchModel.h
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AVCitySearchViewController.h"
#import "AVAsynchronousModel.h"

@class AVCity;

@interface AVCitySearchModel : AVAsynchronousModel

@end
