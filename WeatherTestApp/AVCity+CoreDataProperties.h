//
//  AVCity+CoreDataProperties.h
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import "AVCity+CoreDataClass.h"

@class AVCurrentWeather;
@class AVWeatherForecast;

NS_ASSUME_NONNULL_BEGIN

@interface AVCity (CoreDataProperties)

+ (NSFetchRequest<AVCity *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *name;
@property (nonatomic) BOOL addedByUser;
@property (nonatomic) int64_t cityIdentifier;
@property (nullable, nonatomic, copy) NSString *country;
@property (nullable, nonatomic, retain) AVCurrentWeather *currentWeather;
@property (nullable, nonatomic, retain) NSSet<AVWeatherForecast *> *weatherForecasts;

@end

@interface AVCity (CoreDataGeneratedAccessors)

- (void)addWeatherForecastsObject:(AVWeatherForecast *)value;
- (void)removeWeatherForecastsObject:(AVWeatherForecast *)value;
- (void)addWeatherForecasts:(NSSet<AVWeatherForecast *> *)values;
- (void)removeWeatherForecasts:(NSSet<AVWeatherForecast *> *)values;

@end

NS_ASSUME_NONNULL_END
