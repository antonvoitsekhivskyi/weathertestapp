//
//  AVWeatherForecast+CoreDataProperties.h
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import "AVWeatherForecast+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface AVWeatherForecast (CoreDataProperties)

+ (NSFetchRequest<AVWeatherForecast *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSDate *date;
@property (nullable, nonatomic, retain) AVCity *city;

@end

NS_ASSUME_NONNULL_END
