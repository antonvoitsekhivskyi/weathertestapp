//
//  AVCitiesListModel.m
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import "AVCitiesListModel.h"
#import <CoreData/CoreData.h>
#import "AVCity+CoreDataProperties.h"
#import "AVCitiesListViewController.h"
#import "AppDelegate.h"

@interface AVCitiesListModel ()

@property (nonatomic, strong) NSArray <AVCity *> *cities;
@property (nonatomic, strong) NSString *currentFilter;
@property (nonatomic, weak) AVCitiesListViewController *controller;

@end

@implementation AVCitiesListModel

@synthesize cities;
@synthesize currentFilter;
@synthesize controller;

- (instancetype)initWithController:(AVCitiesListViewController *)aController
{
	self = [super init];
	
	if (nil != self)
	{
		self.controller = aController;
	}
	return self;
}

- (NSPredicate *)filteringPredicate
{
	NSPredicate *thePredicate = nil;
	if (self.currentFilter.length == 0)
	{
		thePredicate = [NSPredicate predicateWithFormat:@"addedByUser == YES"];
	}
	else
	{
		thePredicate = [NSPredicate predicateWithFormat:@"addedByUser == YES AND name BEGINSWITH %@",
				self.currentFilter];
	}
	return thePredicate;
}

- (void)addCity:(AVCity *)aCity
{
	aCity.addedByUser = YES;
	[[self managedObjectContext] save:nil];
	[self filterCitiesUsingString:self.currentFilter];
}

- (void)removeCity:(AVCity *)aCity
{
	aCity.addedByUser = NO;
	[[self managedObjectContext] save:nil];
	[self filterCitiesUsingString:self.currentFilter];
}

@end
