//
//  AVAsynchronousModel.h
//  WeatherTestApp
//
//  Created by Family Mac on 24/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NSManagedObjectContext;
@class AVCity;

@protocol AVUpdatingControllerProtocol <NSObject>

- (void)updateContents;

@end

@interface AVAsynchronousModel : NSObject

@property (nonatomic, readonly, strong) NSArray <AVCity *> *cities;
@property (nonatomic, readonly, strong) NSString *currentFilter;
@property (nonatomic, readonly, weak) id<AVUpdatingControllerProtocol> controller;

- (instancetype)initWithController:(id<AVUpdatingControllerProtocol>)aController;
- (void)filterCitiesUsingString:(NSString *)aFilterString;

- (NSManagedObjectContext *)managedObjectContext;
- (NSPredicate *)filteringPredicate;

@end
