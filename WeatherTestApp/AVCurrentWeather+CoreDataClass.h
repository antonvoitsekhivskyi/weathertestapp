//
//  AVCurrentWeather+CoreDataClass.h
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AVWeather+CoreDataClass.h"

@class AVCity;

extern NSString *const kAVCurrentWeatherEntityName;

NS_ASSUME_NONNULL_BEGIN

@interface AVCurrentWeather : AVWeather

@end

NS_ASSUME_NONNULL_END

#import "AVCurrentWeather+CoreDataProperties.h"
