//
//  AVWeatherForecastModel.m
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import "AVWeatherForecastModel.h"
#import "AVWeatherForecastViewController.h"
#import "AppDelegate.h"

#import "AVWeatherForecast+CoreDataProperties.h"
#import "AVCurrentWeather+CoreDataProperties.h"
#import "AVCity+CoreDataProperties.h"

#import "AVNetworkRequestsManager.h"
#import "AVWeatherRequestBase.h"

@interface AVWeatherForecastModel ()

@property (nonatomic, strong) AVWeatherForecastViewController *controller;
@property (nonatomic, assign) int64_t currentCityIdentifier;
@property (nonatomic, strong) NSMutableDictionary *cachedImages;

@end

@implementation AVWeatherForecastModel

- (instancetype)initWithCity:(AVCity *)aCity controller:
			(AVWeatherForecastViewController *)aController
{
	self = [super init];
	if (nil != self)
	{
		self.city = aCity;
		self.controller = aController;
	}
	return self;
}

- (NSMutableDictionary *)cachedImages
{
	if (nil == _cachedImages)
	{
		_cachedImages = [NSMutableDictionary new];
	}
	return _cachedImages;
}

- (AVCurrentWeather *)currentWeatherForecast
{
	// Update, if the last weather update was earlier than 10 min ago (according to openWeather API
	// documentation: "Do not send requests more then 1 time per 10 minutes from one device/one API
	// key. Normally the weather is not changing so frequently.")
	if (nil == self.city.currentWeather || [self.city.currentWeather.updateDate
				timeIntervalSinceNow] < -600)
	{
		AVNetworkRequestsManager *theRequestsManager = [AVNetworkRequestsManager sharedManager];
		id<AVWeatherRequest> theRequest = [theRequestsManager weatherRequestWithOptions:
					@{AVCityIDOptionsKey: @(self.city.cityIdentifier)}
					type:AVCurrentWeatherRequestType];
		[theRequestsManager scheduleRequest:theRequest delegate:self];
	}
	return self.city.currentWeather;
}

- (AVWeatherForecast *)weatherForecastForDay:(NSUInteger)aDay
{
	AVWeatherForecast *theResult = nil;
	NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:YES];
	NSArray *theSortedForecasts = [self.city.weatherForecasts.allObjects sortedArrayUsingDescriptors:
				@[dateDescriptor]];
	if (aDay - 1 < theSortedForecasts.count)
	{
		theResult = theSortedForecasts[aDay - 1];
	}
	if (![self forecastsAreUpdated])
	{
		AVNetworkRequestsManager *theRequestsManager = [AVNetworkRequestsManager sharedManager];
		id<AVWeatherRequest> theRequest = [theRequestsManager weatherRequestWithOptions:
					@{AVCityIDOptionsKey: @(self.city.cityIdentifier)}
					type:kAVWeatherForecastRequestType];
		[theRequestsManager scheduleRequest:theRequest delegate:self];
	}
	return theResult;
}

- (BOOL)forecastsAreUpdated
{
	BOOL theResult = (self.city.weatherForecasts.count >= 3);
	if (theResult)
	{
		for (AVWeatherForecast *theForecast in self.city.weatherForecasts)
		{
			NSTimeInterval theTimeInterval = [theForecast.updateDate timeIntervalSinceNow];
			if (theTimeInterval < -600)
			{
				theResult = NO;
				break;
			}
		}
	}
	
	return theResult;
}

- (UIImage *)imageForIconName:(NSString *)anIconName
{
	UIImage *theResult = nil;
	if (anIconName.length > 0)
	{
		BOOL theShouldStartDownloading = NO;
		theResult = self.cachedImages[anIconName];

		if (nil == theResult)
		{
			NSString *theDocumentsDirectory = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
						NSUserDomainMask, YES).firstObject;
			NSString *theFullImagePath = [theDocumentsDirectory stringByAppendingPathComponent:
						anIconName];
			theShouldStartDownloading = ![[NSFileManager defaultManager] fileExistsAtPath:
						theFullImagePath];
			if (!theShouldStartDownloading)
			{
				theResult = [UIImage imageWithContentsOfFile:theFullImagePath];
			}
		}
		if (theShouldStartDownloading)
		{
			AVNetworkRequestsManager *theRequestsManager = [AVNetworkRequestsManager sharedManager];
			id<AVWeatherRequest> theRequest = [theRequestsManager weatherRequestWithOptions:
						@{kAVWeatherIconNameOptionsKey: anIconName}
						type:kAVWeatherIconRequestType];
			[theRequestsManager scheduleRequest:theRequest delegate:self];
		}
	}
	return theResult;
}

- (NSManagedObjectContext *)managedObjectContext
{
	return [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
}

#pragma mark - AVRequestDelegate protocol implementation

- (void)request:(id<AVWeatherRequest>)aRequest didFinishWithResult:(id)aResult
			options:(NSDictionary *)anOptions error:(NSError *)anError
{
	if (nil == anError)
	{
		// "theResult" consistency check is skipped for fast delivered test task solution

		if ([aRequest type] == AVCurrentWeatherRequestType)
		{
			NSDictionary *theResult = aResult;
			AVCurrentWeather *theWeather = [NSEntityDescription
						insertNewObjectForEntityForName:kAVCurrentWeatherEntityName
						inManagedObjectContext:[self managedObjectContext]];

			theWeather.humidity = [theResult[@"main"][@"humidity"] integerValue];
			theWeather.pressure = [theResult[@"main"][@"pressure"] floatValue];
			theWeather.temperature = [theResult[@"main"][@"temp"] floatValue];
			theWeather.maxTemperature = [theResult[@"temp"][@"temp_max"] floatValue];
			theWeather.minTemperature = [theResult[@"temp"][@"temp_min"] floatValue];
			theWeather.cloud = theResult[@"weather"][0][@"description"];
			theWeather.iconName = theResult[@"weather"][0][@"icon"];
			theWeather.iconName = [theWeather.iconName stringByAppendingPathExtension:@"png"];
			theWeather.windSpeed = [theResult[@"wind"][@"speed"] floatValue];
			theWeather.updateDate = [NSDate date];
			self.city.currentWeather = theWeather;
		}
		else if ([aRequest type] == kAVWeatherForecastRequestType && [aResult[@"list"] count] >= 4)
		{
			NSArray *theForecasts = aResult[@"list"];
			//skip the first forecast - this is forecast for current day (but it differs from current
			//weather, thus separate "current weather request" is used)
			[self.city removeWeatherForecasts:self.city.weatherForecasts];
			for (NSUInteger theCounter = 1; theCounter < 4; theCounter++)
			{
				AVWeatherForecast *theWeather = [NSEntityDescription
							insertNewObjectForEntityForName:kAVWeatherForecastEntityName
							inManagedObjectContext:[self managedObjectContext]];
				theWeather.humidity = [theForecasts[theCounter][@"humidity"] integerValue];
				theWeather.pressure = [theForecasts[theCounter][@"pressure"] floatValue];
				theWeather.maxTemperature = [theForecasts[theCounter][@"temp"][@"max"] floatValue];
				theWeather.minTemperature = [theForecasts[theCounter][@"temp"][@"min"] floatValue];
				theWeather.cloud = theForecasts[theCounter][@"weather"][0][@"description"];
				theWeather.iconName = theForecasts[theCounter][@"weather"][0][@"icon"];
				theWeather.iconName = [theWeather.iconName stringByAppendingPathExtension:@"png"];
				theWeather.windSpeed = [theForecasts[theCounter][@"speed"] floatValue];
				theWeather.date = [NSDate dateWithTimeIntervalSince1970:
							[theForecasts[theCounter][@"dt"] integerValue]];
				theWeather.updateDate = [NSDate date];
				[self.city addWeatherForecastsObject:theWeather];
			}
		}
		else if ([aRequest type] == kAVWeatherIconRequestType && nil != aResult)
		{
			NSString *theIconName = anOptions[kAVWeatherIconNameOptionsKey];
			self.cachedImages[theIconName] = aResult;
		}
		[[self managedObjectContext] save:nil];
		[self.controller updateWeather];
	}
	else
	{
		[self.controller showErrorAlert];
	}
}

@end
