//
//  AVCitySearchModel.m
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import "AVCitySearchModel.h"
#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import "AVCity+CoreDataProperties.h"

static NSString *const kAVDataBaseMigrationWasPerformed = @"AVDataBaseMigrationWasPerformed";

@interface AVCitySearchModel ()

@property (nonatomic, strong) NSString *currentFilter;

@end

@implementation AVCitySearchModel

@synthesize currentFilter;

- (NSPredicate *)filteringPredicate
{
	return [NSPredicate predicateWithFormat:@"name BEGINSWITH %@", self.currentFilter];
}

@end
