//
//  AVWeatherForecast+CoreDataClass.m
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import "AVWeatherForecast+CoreDataClass.h"
#import "AVCity+CoreDataProperties.h"

NSString *const kAVWeatherForecastEntityName = @"AVWeatherForecast";

@implementation AVWeatherForecast

@end
