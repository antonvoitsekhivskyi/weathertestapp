//
//  AVWeather+CoreDataProperties.h
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import "AVWeather+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface AVWeather (CoreDataProperties)

+ (NSFetchRequest<AVWeather *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *cloud;
@property (nonatomic) int16_t humidity;
@property (nonatomic) float maxTemperature;
@property (nonatomic) float minTemperature;
@property (nonatomic) float pressure;
@property (nullable, nonatomic, copy) NSDate *updateDate;
@property (nonatomic) float windSpeed;
@property (nullable, nonatomic, copy) NSString *iconName;

@end

NS_ASSUME_NONNULL_END
