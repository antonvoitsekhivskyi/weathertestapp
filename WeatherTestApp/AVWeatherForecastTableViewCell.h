//
//  AVWeatherForecastTableViewCell.h
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AVWeatherForecastTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *temperatureLabel;
@property (weak, nonatomic) IBOutlet UILabel *cloudsDescription;
@property (weak, nonatomic) IBOutlet UIImageView *cloudsImageView;
@property (weak, nonatomic) IBOutlet UILabel *preasureLabel;
@property (weak, nonatomic) IBOutlet UILabel *windLabel;
@property (weak, nonatomic) IBOutlet UILabel *humidityLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

- (void)beginIndeterminateProgress;
- (void)stopIndeterminateProgress;

@end
