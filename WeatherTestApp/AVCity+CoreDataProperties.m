//
//  AVCity+CoreDataProperties.m
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import "AVCity+CoreDataProperties.h"

@implementation AVCity (CoreDataProperties)

+ (NSFetchRequest<AVCity *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"AVCity"];
}

@dynamic name;
@dynamic addedByUser;
@dynamic cityIdentifier;
@dynamic country;
@dynamic currentWeather;
@dynamic weatherForecasts;

@end
