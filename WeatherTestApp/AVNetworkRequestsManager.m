//
//  AVNetworkRequestsManager.m
//  WeatherTestApp
//
//  Created by Family Mac on 23/10/16.
//  Copyright © 2016 Anton Voitsekhivskyi. All rights reserved.
//

#import "AVNetworkRequestsManager.h"
#import "AVWeatherRequestBase.h"
#import "AVCurrentWeatherRequest.h"
#import "AVWeatherForecastRequest.h"
#import "AVWeatherIconRequest.h"

NSString *const AVCityIDOptionsKey = @"AVCityID";
NSString *const kAVWeatherIconNameOptionsKey = @"AVWeatherIconNameOption";
NSString *const kAVAPIToken = @"547092fa11c9207a9297c72ba52d616f";

static NSTimeInterval const kAVRepeatDelay = 5.0;

@interface AVNetworkRequestsManager ()

@property (nonatomic, strong) NSMutableArray *scheduledRequests;

@end

@implementation AVNetworkRequestsManager

+ (instancetype)sharedManager
{
	static dispatch_once_t theExecutionToken = 0;
	__strong static AVNetworkRequestsManager *theSharedManager = nil;
	
	dispatch_once(&theExecutionToken,
	^{
		theSharedManager = [self new];
	});
	
	return theSharedManager;
}

- (NSMutableArray *)scheduledRequests
{
	if (nil == _scheduledRequests)
	{
		_scheduledRequests = [NSMutableArray new];
	}
	return _scheduledRequests;
}

- (void)scheduleRequest:(id<AVWeatherRequest>)aRequest delegate:(id<AVRequestDelegate>)aDelegate
{
	BOOL theShouldSchedule = YES;
	for (id<AVWeatherRequest> theRequest in self.scheduledRequests)
	{
	// Do not schedule the same request, if there is one already scheduled
		if ([theRequest isEqualToRequest:aRequest])
		{
			theShouldSchedule = NO;
			break;
		}
	}
	if (theShouldSchedule)
	{
		[self.scheduledRequests addObject:aRequest];
		aRequest.delegate = aDelegate;
		[aRequest start];
	}
}

- (id<AVWeatherRequest>)weatherRequestWithOptions:(NSDictionary *)anOptions type:
			(AVRequestType)aType
{
	id<AVWeatherRequest> theRequest = nil;
	switch (aType)
	{
		case AVCurrentWeatherRequestType:
		{
			theRequest = [[AVCurrentWeatherRequest alloc] initWithOptions:anOptions requestsManager:
						self];
			break;
		}
		case kAVWeatherForecastRequestType:
		{
			theRequest = [[AVWeatherForecastRequest alloc] initWithOptions:anOptions requestsManager:
						self];
			break;
		}
		case kAVWeatherIconRequestType:
		{
			theRequest = [[AVWeatherIconRequest alloc] initWithOptions:anOptions requestsManager:
						self];
			break;
		}
		default:
		{
			break;
		}
	}

	return theRequest;
}

- (void)didFinishRequest:(id<AVWeatherRequest>)aRequest
{
	[self.scheduledRequests removeObject:aRequest];
}

- (void)repeatAfterDelayProcess:(id<AVWeatherRequest>)aRequest
{
	dispatch_async(dispatch_get_main_queue(),
	^{
		[(id)aRequest performSelector:@selector(start) withObject:nil afterDelay:kAVRepeatDelay];
	});
}

@end

@implementation NSURLSession (ErrorTypeCategory)

+ (BOOL)isBadConnectionError:(NSError *)anError
{
	NSArray *theNeedsToRepeatErrors = @[@(NSURLErrorUnknown), @(NSURLErrorTimedOut),
			@(NSURLErrorCannotConnectToHost), @(NSURLErrorNetworkConnectionLost),
			@(NSURLErrorDNSLookupFailed), @(NSURLErrorResourceUnavailable),
			@(NSURLErrorNotConnectedToInternet), @(NSURLErrorBadServerResponse),
			@(NSURLErrorCallIsActive), @(NSURLErrorDataNotAllowed),
			@(NSURLErrorSecureConnectionFailed), @(NSURLErrorCannotLoadFromNetwork)];

	return (NSNotFound != [theNeedsToRepeatErrors indexOfObject:@(anError.code)]);
}

@end
